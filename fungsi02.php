<style>
	body {
		background-color: deeppink;
	}
	
</style>
<body>
	<?php
	//fungsi ini dengan return value dan parameter
	function cetak_ganjil($awal,$akhir){
		for($i = $awal; $i < $akhir; $i++){
			if ($i%2==1) {
				echo "$i, ";
			}
		}

	}
	//pemanggilan fungsi
	$a=10;
	$b=50;
	echo "<b>Bilangan Ganjil dari $a sampai $b adalah : </b><br>";
	cetak_ganjil($a,$b);
	?>	
</body>

